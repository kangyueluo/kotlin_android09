package com.javaclass.anima.android06progress

import android.app.ProgressDialog
import android.content.DialogInterface
import android.os.Handler
import android.os.Message
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button

class MainActivity : AppCompatActivity() {

    private var progress1: Button? = null
    private var progress2: Button? = null
    private var pDailog1: ProgressDialog? = null
    private var pDialog2: ProgressDialog? = null
    private var handler: UiHandler? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        handler = UiHandler()
        progress1 = findViewById(R.id.progress1) as Button
        progress2 = findViewById(R.id.progress2) as Button

        pDailog1 = ProgressDialog(this, ProgressDialog.THEME_DEVICE_DEFAULT_DARK)
        pDailog1!!.setMessage(" 執行中...")
        pDailog1!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        pDailog1!!.setButton(ProgressDialog.BUTTON_NEGATIVE, "取消") { dialogInterface, i -> pDailog1!!.dismiss() }

        pDialog2 = ProgressDialog(this, ProgressDialog.THEME_HOLO_LIGHT)
        pDialog2!!.setMessage("下載中...")
        pDialog2!!.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL)

        progress1!!.setOnClickListener { ShowProgress1() }

        progress2!!.setOnClickListener { ShowProgress2() }
    }

    private fun ShowProgress1() {
        MyTask1().start()
    }

    private fun ShowProgress2() {
        MyTask2().start()
    }

    private inner class MyTask1 : Thread() {

        override fun run() {
            super.run()
            handler!!.sendEmptyMessage(1)
            try {
                Thread.sleep(5000)

            } catch (e: Exception) {
                e.printStackTrace()
            }

            handler!!.sendEmptyMessage(2)
        }
    }

    inner class MyTask2 : Thread() {
        override fun run() {
            super.run()

            handler!!.sendEmptyMessage(3)
            for (i in 0..99) {
                try {
                    val message = Message()
                    message.what = 5
                    val bundle = Bundle()
                    bundle.putInt("progress", i)
                    message.data = bundle
                    handler!!.sendMessage(message)

                    Thread.sleep(100)
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
            handler!!.sendEmptyMessage(4)
        }
    }

    private inner class UiHandler : Handler() {
        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)

            when (msg.what) {
                1 -> pDailog1!!.show()
                2 -> pDailog1!!.dismiss()
                3 -> {
                    pDialog2!!.max = 100
                    pDialog2!!.show()
                }
                4 -> if (pDialog2!!.isShowing) {
                    pDialog2!!.dismiss()
                }
                5 -> if (pDialog2!!.isShowing) {
                    pDialog2!!.progress = msg.data.getInt("progress")
                }
            }
        }
    }

}
